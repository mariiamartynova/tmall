from tmall_json import tmall_dict
import yaml
# TODO replace ['0'] -> iterator
# '尺码'.encode('unicode_escape')
# api_stack = tmall_dict['data']['apiStack']
# for api_item in api_stack:
#     yaml.load(api_item['value'])
api_stack = yaml.load(tmall_dict['data']['apiStack'][0]['value'])
trackparams = api_stack['params']['trackParams']
mock_data = yaml.load(tmall_dict['data']['mockData'])
product_id = tmall_dict['data']['item']['itemId']
store_id = tmall_dict['data']['seller']['shopId']
product_name = tmall_dict['data']['item']['title']  # need to be decoded
subtitle = tmall_dict['data']['item']['subtitle']
additional_images = tmall_dict['data']['item']['images']
images = []
for image in additional_images:
    images.append(image.replace('//img', 'img'))
additional_videos = tmall_dict['data']['item']['videos']
videos = []
for video in additional_videos:
    videos.append(video)
category_id = tmall_dict['data']['item']['categoryId']
root_category_id = tmall_dict['data']['item']['rootCategoryId']
main_img = images[0]
group_properties = tmall_dict['data']['props']['groupProps']  # need to be decoded
discounted_price = api_stack['skuCore']['sku2info']['0']['price']['priceText']
festival_price = ''
original_price = mock_data['price']['price']['priceText']
sku_inventory = api_stack['skuCore']['sku2info']['0']['quantity']
sku_data = tmall_dict['data']['skuBase']
sku_ids = []
for sku_id in sku_data['skus']:
    sku_ids.append(sku_id['skuId'])
# sku_id = trackparams['skuId']
sku_names = []
sku_images = []
for sku_value in sku_data['props']:
    for sku_name in sku_value['values']:
        sku_names.append(sku_name['name'])
        sku_images.append(sku_name['image'])
try:
    total_reviews = tmall_dict['data']['item']['commentCount']
except (KeyError, TypeError):
    total_reviews = tmall_dict['data']['item']['feedAllCount']  # TODO
favourites_count = tmall_dict['data']['item']['favcount']
try:
    past_30_days_sales = api_stack['item']['sellCount']
except KeyError:
    past_30_days_sales = ''
try:
    total_sales = tmall_dict['data']['item']['sold'] or tmall_dict['data']['item']['sold']
except(KeyError, TypeError):
    total_sales = ''
total_inventory = mock_data['skuCore']['sku2info']['0']['quantity']
# total_reviews = ''
# product_id = ''
# product_title = ''
bool_shop_prom = True
try:
    shop_proms = api_stack['price']['shopProm']
except KeyError:
    promotion_activity_id = ''
    promotion_product_id = ''
    promotion_content = ''
    promotion_group_id = ''
    promotion_icon_text = ''
    promotion_period_start = ''
    promotion_period_end = ''
    promotion_title = ''
    promotion_type = ''
    bool_shop_prom = False
if bool_shop_prom:
    prom_activityId = []
    prom_content = []
    prom_groupId = []
    prom_iconText = []
    prom_start_period = []
    prom_end_period = []
    prom_title = []
    prom_type = []
    for shop_prom in shop_proms:
        prom_activityId.append(shop_prom['activityId'])
        promotion_product_id = ''
        prom_content.append(shop_prom['content'])
        prom_groupId.append(shop_prom['groupId'])
        prom_iconText.append(shop_prom['iconText']['content'])
        prom_start_period.append(shop_prom['period'].split('-')[0])
        prom_end_period.append(shop_prom['period'].split('-')[1])
        prom_title.append(shop_prom['title'])
        prom_type.append(shop_prom['type'])
