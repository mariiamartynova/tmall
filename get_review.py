from review_json import review_dict

review_list = review_dict['data']['rateList']
for review in review_list:
    feedback = review['feedback']
    feedback_date = review['feedbackDate']
    feedback_id = review['id']
    feedback_read_count = review['interactInfo']['readCount']
    feedback_reply = review['reply']
    feedback_head_frame_url = review['headFrameUrl']
    feedback_user_nick = review['userNick']
    feedback_sku_map = review['skuMap']
    feedback_share_cover = review['share']['shareCover']
    feedback_sku_id = review['skuId']
    feedback_user_star = review['userStar']
    feedback_repeat_business = review['repeatBusiness']